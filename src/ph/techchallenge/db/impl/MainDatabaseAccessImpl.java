package ph.techchallenge.db.impl;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;

import ph.techchallenge.bean.ERUser;
import ph.techchallenge.db.MainDatabaseAccess;
import ph.techchallenge.db.patients.PatientER;

public class MainDatabaseAccessImpl implements MainDatabaseAccess {

	private String databasePath = "mongodb://localhost:27017";
	private String databaseName = "TechChallenge";
	private String collectionName = "Hospitals";

	private MongoClient mongoClient;
	private MongoDatabase database;
	private MongoCollection<Document> collection;

	private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyy HH:mm:ss", new Locale("us"));

	public MainDatabaseAccessImpl() {
	}

	private void initializeMongoClient() throws UnknownHostException {
		mongoClient = new MongoClient(new MongoClientURI(databasePath));
	}

	private void connectToDB() {
		database = mongoClient.getDatabase(databaseName);
	}

	private void connectToCollection() {
		collection = database.getCollection(collectionName);
	}

	@Override
	public boolean initializeDatabase() {
		try {
			initializeMongoClient();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		connectToDB();
		connectToCollection();
		return true;
	}

	@Override
	public String getHospitalSID(String _name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHospitalName(String _sid) {
		FindIterable<Document> hospitals = collection.find(getHospitalQuery(_sid));
		return hospitals.first().getString("name");
	}

	@Override
	public List<String> getHospitals() {
		FindIterable<Document> hospitals = collection.find();
		List<String> list = new ArrayList<String>();
		for (Document hosp : hospitals) {
			list.add(hosp.get("name").toString());
		}
		return list;
	}

	@Override
	public int nextUserERid(String _hospitalSID) {
		Document dbObj = getHospital(_hospitalSID);

		return getHighestERNumberInHospital(dbObj) + 1;
	}

	private int getHighestERNumberInHospital(Document _dbObjHosp) {
		List<Document> patientsList = (List<Document>) _dbObjHosp.get("patients");

		if (patientsList == null || patientsList.size() == 0) {
			return 0;
		}

		int currentHighest = 0;
		for (Document patient : patientsList) {
			if (currentHighest < (int) patient.get("erNumber")) {
				currentHighest = (int) patient.get("erNumber");
			}
		}
		return currentHighest;
	}

	/**
	 * returns list with all patients in an ER sorted by treatment begin time
	 * (ascending).
	 * 
	 * @param _hospitalSID
	 * @return
	 */
	private List<PatientER> getAllPatientsInER(String _hospitalSID) {
		Document dbObjHosp = getHospital(_hospitalSID);
		List<Document> patientsList = (List<Document>) dbObjHosp.get("patients");

		// add all patients to list
		List<PatientER> patientsER = new ArrayList<PatientER>();
		DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", new Locale("us"));
		for (Document patient : patientsList) {
			try {
				patientsER.add(new PatientER(patient.get("userUUID").toString(), (int) patient.get("erNumber"),
						dateFormat.parse(patient.get("registerTime").toString()),
						dateFormat.parse(patient.get("treatBeginTime").toString()),
						Integer.parseInt(patient.get("estTreatTime").toString())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// sort by date
		Collections.sort(patientsER, new Comparator<PatientER>() {
			@Override
			public int compare(PatientER lhs, PatientER rhs) {
				if (lhs.estTreatBeginTime.getTime() < rhs.estTreatBeginTime.getTime())
					return -1;
				else if (lhs.estTreatBeginTime.getTime() == rhs.estTreatBeginTime.getTime())
					return 0;
				else
					return 1;
			}
		});

		return patientsER;
	}

	private Document getHospital(String _hospitalSID) {
		return collection.find(getHospitalQuery(_hospitalSID)).first();
	}

	private Document getHospitalQuery(String _hospitalSID) {
		return new Document("sid", _hospitalSID);
	}

	/**
	 * 
	 * @param _hospitalSID
	 * @param _estTreatMin estimated treatment time in minutes
	 */
	private Date getLatestTreatBegin(String _hospitalSID, int _estTreatMin) {
		List<PatientER> patients = getAllPatientsInER(_hospitalSID);
		Calendar cal = Calendar.getInstance();

		if (patients.size() == 0) {
			// return current time plus est treat time
			cal.setTime(new Date());
			cal.add(Calendar.MINUTE, 15);
		} else {
			PatientER lastPat = patients.get(patients.size() - 1);
			cal.setTime(lastPat.estTreatBeginTime);
			cal.add(Calendar.MINUTE, lastPat.estTreatTime);

			// if the last treatment is already over
			if (treatmentAlreadyOver(cal)) {
				cal.setTime(new Date());
				cal.add(Calendar.MINUTE, 15);
			}
		}

		return cal.getTime();
	}

	private boolean treatmentAlreadyOver(Calendar _cal) {
		Calendar currentTime = Calendar.getInstance();
		currentTime.setTime(new Date());
		if (_cal.before(currentTime)) {
			return true;
		}
		return false;
	}

	public List<ERUser> getAllCurrentERUsers(String _hospitalSID) {
		List<PatientER> patients = getAllPatientsInER(_hospitalSID);
		List<ERUser> users = new ArrayList<ERUser>();
		Calendar cal = Calendar.getInstance();
		String hospitalName = getHospitalName(_hospitalSID);
		
		// this could be optimized...
		for(PatientER patient : patients) {
			cal.setTime(patient.estTreatBeginTime);
			cal.add(Calendar.MINUTE, patient.estTreatTime);
			if(treatmentAlreadyOver(cal) == false) {
				ERUser user = new ERUser(patient.UUID, _hospitalSID,
						hospitalName, patient.erNumber, patient.estTreatTime,
						format.format(patient.estTreatBeginTime));
				users.add(user);
			}
		}
		
		return users;
	}

	@Override
	public ERUser registerInER(String _hospitalSID, String _userUUID, int _estTreatmentTime) {
		try {
			int userERId = nextUserERid(_hospitalSID);

			// get predicted treatment begin
			Date registerTime = new Date();
			Date treatBegin = getLatestTreatBegin(_hospitalSID, _estTreatmentTime);

			Document patient = new Document();
			patient.append("erNumber", userERId);
			patient.append("userUUID", _userUUID);
			patient.append("estTreatTime", _estTreatmentTime);
			patient.append("registerTime", registerTime.toString());
			patient.append("treatBeginTime", treatBegin.toString());
			
			collection.updateOne(getHospitalQuery(_hospitalSID), Updates.push("patients", patient));
			return new ERUser(_userUUID, _hospitalSID, getHospitalName(_hospitalSID), userERId, _estTreatmentTime,
					format.format(treatBegin));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ERUser registerInER(String _hospitalSID, String _userUUID) {
		return registerInER(_hospitalSID, _userUUID, 20);
	}

	@Override
	public boolean unregisterFromER(String _hospitalSID, String _userUUID) {
		Document patient = new Document();
		patient.append("userUUID", _userUUID);
		System.out.println("userSID = " + _userUUID);
		UpdateResult result = collection.updateOne(getHospitalQuery(_hospitalSID), Updates.pull("patients", patient));
		return result.wasAcknowledged();
	}
}

package ph.techchallenge.db;

import java.util.List;

import ph.techchallenge.bean.ERUser;

public interface MainDatabaseAccess {
	public boolean initializeDatabase();
	public String getHospitalSID(String _name);
	public String getHospitalName(String _sid);
	public List<String> getHospitals();
	public int nextUserERid(String _hospitalSID);
	
	public List<ERUser> getAllCurrentERUsers(String _hospitalSID);
	
	/**
	 * register user in ER.
	 * 
	 * returns null on error
	 * @param _hospitalSID
	 * @param _userUUID
	 * @param _estTreatmentTime
	 * @return
	 */
	public ERUser registerInER(String _hospitalSID, String _userUUID, int _estTreatmentTime);
	/**
	 * register user in ER.
	 * returns user er id;
	 * returns -1 on error
	 * @param _hospitalSID
	 * @param _userUUID
	 * @return
	 */
	public ERUser registerInER(String _hospitalSID, String _userUUID);
	
	public boolean unregisterFromER(String _hospitalSID, String _userUUID);
}

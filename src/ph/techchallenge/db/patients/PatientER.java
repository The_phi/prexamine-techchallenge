package ph.techchallenge.db.patients;

import java.util.Date;

public class PatientER extends Patient {
	public int erNumber;
	public Date registrationTime;
	public Date estTreatBeginTime;
	// estimated treatment time in minutes
	public int estTreatTime;
	
	public PatientER(String _UUID, int _erNumber, Date _registrationTime, Date _estTreatBeginTime, int _estTreatTime) {
		super(_UUID);
		erNumber = _erNumber;
		registrationTime = _registrationTime;
		estTreatBeginTime = _estTreatBeginTime;
		estTreatTime = _estTreatTime;
	}

}

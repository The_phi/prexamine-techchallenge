package ph.techchallenge.controller;

import java.net.UnknownHostException;
import java.time.Duration;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.session.data.mongo.JdkMongoSessionConverter;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClients;


@Configuration
@EnableMongoHttpSession
public class SessionConfig extends AbstractMongoConfiguration{
	
	private String mongo_url = "127.0.0.1:27017";

    @Bean
    public JdkMongoSessionConverter jdkMongoSessionConverter() {
            return new JdkMongoSessionConverter(Duration.ofMinutes(30)); 
    }


//    @Bean
//    public Mongo mongo() throws UnknownHostException {
////        mongo_url = "127.0.0.1:27017";// "dev-ngcsc:27017,dev1-ngcsc:27017,dev2-ngcsc:27017";
//        String url = mongo_url;
//        ArrayList<ServerAddress> addr = new ArrayList<ServerAddress>();
//        for (String s : url.split(",")) {
//            addr.add(new ServerAddress(s));
//        }
//        Mongo mongo = new Mongo(addr);
//        System.out.println("mongobean");
//        return mongo;
//    }


    @Override
	protected String getDatabaseName() {
		return "TechChallenge";
	}

	@Override
	public MongoClient mongoClient() {
		return new MongoClient("localhost", 27017);
	}
}
package ph.techchallenge.controller;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.techchallenge.bean.ERUser;
import ph.techchallenge.db.MainDatabaseAccess;
import ph.techchallenge.db.impl.MainDatabaseAccessImpl;

@Controller
public class TechChallengeController {

	private MainDatabaseAccess databaseAccess;

	@PostConstruct
	public void init() {
		databaseAccess = new MainDatabaseAccessImpl();
		databaseAccess.initializeDatabase();

		System.out.println("TechChallengeController init");
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String printHello(ModelMap model) {
		model.addAttribute("message", "Hello Spring MVC Framework!");

		databaseAccess.registerInER("733aafb3-1082-4278-a89c-a3a6bc91c240", "blabal",
				databaseAccess.nextUserERid("733aafb3-1082-4278-a89c-a3a6bc91c240"));
		return "welcome";
	}

	@RequestMapping(value = "/ajaxRegisterUserInER", method = RequestMethod.POST)
	public @ResponseBody ERUser ajaxRegisterUserInER(HttpServletRequest request, HttpServletResponse response) {
		String hospitalSID = request.getParameter("sid");
		if(hospitalSID == null) {
			return null;
		}
		
		UUID userUUID = UUID.randomUUID();
		return registerUserInER(userUUID, hospitalSID);
	}
	
	@RequestMapping(value = "/ajaxUnregisterUserFromER", method = RequestMethod.POST)
	public @ResponseBody String ajaxUnregisterUserFromER(HttpServletRequest request, HttpServletResponse response) {
		String hospitalSID = request.getParameter("hospitalSID");
		if(hospitalSID == null) {
			return null;
		}
		String userSID = request.getParameter("userSID");
		if(userSID == null) {
			return null;
		}
		
		databaseAccess.unregisterFromER(hospitalSID, userSID);
		
		return "success";
	}
	
	@RequestMapping(value = "/ajaxGetAllUsersInER", method = RequestMethod.POST)
	public @ResponseBody List<ERUser> ajaxGetAllUsersInER(HttpServletRequest request, HttpServletResponse response) {
		String hospitalSID = request.getParameter("hospitalSID");
		if(hospitalSID == null) {
			return null;
		}
		return databaseAccess.getAllCurrentERUsers(hospitalSID);
	}		
	
	
	
	/**
	 * registers user in ER;
	 * returns user er id
	 * @param _userID
	 * @param _hospitalSID
	 * @return
	 */
	private ERUser registerUserInER(UUID _userID, String _hospitalSID) {
		return databaseAccess.registerInER(_hospitalSID, _userID.toString());
	}

//	// test for sessions
//	@GetMapping("/test")
//	public ResponseEntity<Integer> count(HttpSession session) {
//
//		Integer counter = (Integer) session.getAttribute("count");
//
//		if (counter == null) {
//			counter = 1;
//		} else {
//			counter++;
//		}
//
//		session.setAttribute("count", counter);
//
//		System.out.println("hell yeah " + counter);
//
//		return ResponseEntity.ok(counter);
//	}
}

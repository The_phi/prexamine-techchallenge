package ph.techchallenge.bean;

import java.util.Date;

public class ERUser {
	private String userUUID;
	private String hospitalUUID;
	private String hospitalName;
	private int userERNumber;
	private int estTeatTime;
	private String estTreatBegin; 

	public ERUser(String userUUID, String hospitalUUID, String hospitalName, int userERNumber, int estTeatTime, String _estTreatBegin) {
		super();
		this.userUUID = userUUID;
		this.hospitalUUID = hospitalUUID;
		this.hospitalName = hospitalName;
		this.userERNumber = userERNumber;
		this.estTeatTime = estTeatTime;
		this.estTreatBegin = _estTreatBegin;
	}

	public String getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}

	public String getHospitalUUID() {
		return hospitalUUID;
	}

	public void setHospitalUUID(String hospitalUUID) {
		this.hospitalUUID = hospitalUUID;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public int getUserERNumber() {
		return userERNumber;
	}

	public void setUserERNumber(int userERNumber) {
		this.userERNumber = userERNumber;
	}

	public int getEstTeatTime() {
		return estTeatTime;
	}

	public void setEstTeatTime(int estTeatTime) {
		this.estTeatTime = estTeatTime;
	}

	public String getEstTreatBegin() {
		return estTreatBegin;
	}

	public void setEstTreatBegin(String estTreatBegin) {
		this.estTreatBegin = estTreatBegin;
	}
}

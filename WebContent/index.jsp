<!doctype  html>
<html lang="de">
<head>
<meta charset="utf-8" />
<title>Tech Challenge</title>

<!-- needed so that bootstrap works properly on mobile devices -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">


<!-- Load bootstrap and jquery -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- load own javascript -->
<script src="/TechChallenge/js/main.js"></script>
<script src="/TechChallenge/js/jquery/jquery.min.js"></script>
<script src="/TechChallenge/js/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- load own css -->
<link rel="stylesheet" href="/TechChallenge/css/main.css">
<link rel="stylesheet" href="/TechChallenge/css/freelancer.css">


</head>
<!-- <body style="background-color: #00BFFF;"> -->
<body style="">
	<nav
		class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top"
		id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger" href="/TechChallenge/"> <img
				alt="Prexamine logo" src="/TechChallenge/img/preXamine LogoNeu.png"
				height="50px">
			</a>
			<button
				class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
				type="button" data-toggle="collapse" data-target="#navbarResponsive"
				aria-controls="navbarResponsive" aria-expanded="false"
				aria-label="Toggle navigation">
				Men� <i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item mx-0 mx-lg-1"><a
						class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
						href="/TechChallenge/pages/wartezimmer.html"
						href="#login">Anmelden</a></li>
					<li class="nav-item mx-0 mx-lg-1"><a
						class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
						href="#about">�ber uns</a></li>
					<li class="nav-item mx-0 mx-lg-1"><a
						class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
						href="#contact">Kontakt</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<header class="masthead text-center" id="login">
		<div class="container d-flex align-items-center flex-column">

			<h2
				class="page-section-heading text-center text-uppercase text-secondary">Jetzt
				in die Notaufnahme einchecken</h2>

			<!-- Icon Divider -->
			<div class="divider-custom">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-line"></div>
			</div>
			<button type="button" class="btn btn-warning btn-lg"
				onclick="window.location='/TechChallenge/pages/wartezimmer.html';">Einchecken</button>
		</div>
	</header>

	<!-- Vision Section -->
	<section class="page-section bg-primary text-white mb-0" id="vision">
		<div class="container">

			<!-- About Section Heading -->
			<h2
				class="page-section-heading text-center text-uppercase text-white">So funktioniert's</h2>

			<!-- Icon Divider -->
			<div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-line"></div>
			</div>

			<!-- About Section Content -->
			<div class="row">
				<div class="col-md-12">
					<video controls style="width: 100%">
						<source src="/TechChallenge/video/preXamineVision.mp4" type="video/mp4">
						Your browser does not support the video tag.
					</video>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Note Section -->
	<section class="page-section mb-0" id="note">
		<div class="container">

			<!-- Note Section Heading -->
			<h2
				class="page-section-heading text-center text-uppercase text-secondary">Bitte beachten</h2>

			<!-- Icon Divider -->
			<div class="divider-custom">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-line"></div>
			</div>

			<!-- Note Section Content -->
			<div class="row">
				<div class="col-lg-4 ml-auto">
					<p class="lead">Sie k�nnen sich �ber diese Website bei einem
						Krankenhaus Ihrer Wahl online in der Notaufnahme registrieren. Das
						erm�glicht Ihnen, Ihre Wartezeit frei zu gestalten.</p>
				</div>
				<div class="col-lg-4 mr-auto">
					<p class="lead">Allerdings kann sich diese durch akute Notf�lle
						oder andere Ereignisse sowohl verk�rzen als auch verl�ngern.
						Stellen Sie daher bitte sicher, die aktuelle Wartezeit stets im
						Auge zu behalten und eine halbe Stunde vor Behandlungsbeginn im
						Krankenhaus einzutreffen.</p>
				</div>
			</div>
		</div>
	</section>

	<!-- About Section -->
	<section class="page-section bg-primary text-white mb-0" id="about">
		<div class="container">

			<!-- About Section Heading -->
			<h2 class="page-section-heading text-center text-uppercase text-white">�ber
				uns</h2>

			<!-- Icon Divider -->
			<div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-line"></div>
			</div>

			<!-- About Section Content -->
				<h4 class="text-center" style="padding-bottom: 20px;">Unsere Vision ist sinnloses Zeitabsitzen in
					Wartezimmern zu eliminieren.</h4>

			<div class="row">
				<div class="col-md-3 text-center mobile-space">
					<img alt="Leo" src="/TechChallenge/img/Leo.png" class="imgAbout"
						style="padding-bottom: 20px;">
					<h4>Leo</h4>
					<p>Strategy & Business Development</p>
				</div>
				<div class="col-md-3 text-center mobile-space">
					<img alt="Antonia" src="/TechChallenge/img/Antonia.png"
						class="imgAbout" style="padding-bottom: 20px;">
					<h4>Antonia</h4>
					<p>Marketing & PR</p>
				</div>
				<div class="col-md-3 text-center mobile-space">
					<img alt="Meike" src="/TechChallenge/img/Meike.png"
						class="imgAbout" style="padding-bottom: 20px;">
					<h4>Meike</h4>
					<p>Organisation & Structure</p>
				</div>
				<div class="col-md-3 text-center">
					<img alt="Phillip" src="/TechChallenge/img/Phillip.png"
						class="imgAbout" style="padding-bottom: 20px;">
					<h4>Phillip</h4>
					<p>Inventor of the color blue</p>
				</div>
			</div>
		</div>
	</section>

	<!-- Contact Section -->
	<section class="page-section" id="contact">
		<div class="container">

			<!-- Contact Section Heading -->
			<h2
				class="page-section-heading text-center text-uppercase text-secondary mb-0">Kontaktiere
				uns</h2>

			<!-- Icon Divider -->
			<div class="divider-custom">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-line"></div>
			</div>

			<!-- Contact Section Form -->
			<div class="row">
				<div class="col-lg-8 mx-auto">
					<!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
					<form name="sentMessage" id="contactForm" novalidate="novalidate">
						<div class="control-group">
							<div
								class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Name</label> <input class="form-control" id="name"
									type="text" placeholder="Name" required="required"
									data-validation-required-message="Bitte Name eingeben">
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="control-group">
							<div
								class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Email Adresse</label> <input class="form-control"
									id="email" type="email" placeholder="Email Adresse"
									required="required"
									data-validation-required-message="Bitte gib deine Email Adresse ein.">
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="control-group">
							<div
								class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Telefonnummer</label> <input class="form-control"
									id="phone" type="tel" placeholder="Telefonnummer"
									required="required"
									data-validation-required-message="Bitte gib deine Telefonnummer ein.">
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="control-group">
							<div
								class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Nachricht</label>
								<textarea class="form-control" id="message" rows="5"
									placeholder="Nachricht" required="required"
									data-validation-required-message="Gib hier deine Nachricht ein."></textarea>
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<br>
						<div id="success"></div>
						<div class="form-group">
							<button type="submit" class="btn btn-lg btn-warning"
								id="sendMessageButton">Senden</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</section>


	<!-- Copyright Section -->
	<section class="copyright py-4 text-center text-white">
		<div class="container">
			<small>Copyright &copy; Prexamine 2019</small>
		</div>
	</section>

	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
	<div class="scroll-to-top d-lg-none position-fixed ">
		<a class="js-scroll-trigger d-block text-center text-white rounded"
			href="#page-top"> <i class="fa fa-chevron-up"></i>
		</a>
	</div>
	
	
	<script src="/TechChallenge/js/jquery-easing/jquery.easing.min.js"></script>
	<script src="/TechChallenge/js/freelancer.min.js"></script>
	
</body>

</html>
var waitingRoom = (function() {
	// estimated waiting time in seconds
	var estWaitTime = 120 * 60;

	var $EROptionsContainer;
	var $ERContainer;

	var $mainCountdown;
	var $treatmentBegin;

	var registrationTime = new Date();
	var estTreatmentDate = new Date();

	var userSID = null;
	var hospitalSID = null;
	var hospitalName = null;
	var userERNumber = null;
	var interval;

	function init() {
		$EROptionsContainer = $('#EROptionsContainer');
		$ERContainer = $('#ERContainer');

		$mainCountdown = $('#mainCountdown');
		$treatmentBegin = $('#treatmentBegin');

		registrationTime = new Date();
		estimatedTreatmenttime = new Date();

		if (checkAlreadyRegistered()) {
			alreadyRegistered();
		}

	}

	function checkAlreadyRegistered() {
		if (Cookies.get("userSID") != undefined) {
			return true;
		}
	}

	function alreadyRegistered() {
		var ERUser = new Object();
		ERUser.hospitalName = Cookies.get("hospitalName");
		ERUser.hospitalUUID = Cookies.get("hospitalSID");
		ERUser.userERNumber = Cookies.get("userERNumber");
		ERUser.userUUID = Cookies.get("userSID");
		ERUser.estTreatBegin = Cookies.get("estTreatBegin");

		register(ERUser);

		showAlreadyRegisteredDialogue();
	}

	function showAlreadyRegisteredDialogue() {
		alert("Sie sind bereits in einer Notaufnahme registriert.");
	}

	/*
	 * ERUser example: estTeatTime: 20 estTreatBegin: "Mon Jun 17 16:06:00 CEST
	 * 2019" hospitalName: "Klinikum rechts der Isar" hospitalUUID:
	 * "733aafb3-1082-4278-a89c-a3a6bc91c240" userERNumber: 5 userUUID:
	 * "54303bf9-b3a5-483b-b3ba-58cd5d76dbc4"
	 * 
	 * see ph.techchallenge.bean.ERUser
	 */
	function register(ERUser) {
		var estTreatTimeStr = ERUser.estTreatBegin;

		var estTreatTime = moment(estTreatTimeStr, "DD MMM YYYY HH:mm:ss")
				.toDate();
		$mainCountdown.timeTo(estTreatTime, function() {
		});

		// set time text
		$treatmentBegin.text(estTreatTime.toLocaleTimeString() + " Uhr");

		userSID = ERUser.userUUID;
		hospitalSID = ERUser.hospitalUUID;
		userERNumber = ERUser.userERNumber;
		// save cookies
		createERCookies(userSID, hospitalSID, ERUser.userERNumber,
				decodeURIComponent(ERUser.hospitalName), 2 * 60 * 60,
				estTreatTimeStr);

		// show correct map
		$('#mapThingy').attr('src', getCorrectGoogleMapsSrc(hospitalSID));

		// hide hospital menu
		$EROptionsContainer.hide();
		$ERContainer.show();

		// set er number
		$("#ERNumber").text(ERUser.userERNumber);

		getAllERUsers();

		interval = setInterval(getAllERUsers, 300000);
		setupAlertTimerReminder(diffMinutes(estTreatTime, new Date()) - 15);
	}

	// delay should be in minutes
	function setupAlertTimerReminder(delay) {

		if (delay < 5) {
			console.log("zu wenig warte zeit");
		} else if (delay < 10) {
			alert("Bitte begeben Sie sich zur Notaufnahme.");
		} else {
			setTimeout(
					function() {
						alert("Vergessen Sie bitte nicht, sich in naechster Zeit in die Notaufnahme zu begeben, um Verzoegerungen zu vermeiden.");
					}, delay * 1000 * 60);
		}

	}

	// get minute diff
	function diffMinutes(dt2, dt1) {

		var diff = (dt2.getTime() - dt1.getTime()) / 1000;
		diff /= 60;
		return Math.abs(Math.round(diff));

	}

	function unregister() {
		deleteAllERCookies();

		$EROptionsContainer.show();
		$ERContainer.hide();

		clearInterval(interval);
		alert("Erfolgreich aus der Notaufnahme abgemeldet");
	}

	function sendRegisterRequest(hospitalSID) {
		$.ajax({
			type : "POST",
			url : "/TechChallenge/ajaxRegisterUserInER",
			data : {
				sid : hospitalSID
			}, // parameters
			success : function(response) {
				waitingRoom.register(response);
			}
		});
	}

	function sendUnregisterRequest() {
		$.ajax({
			type : "POST",
			url : "/TechChallenge/ajaxUnregisterUserFromER",
			data : {
				hospitalSID : hospitalSID,
				userSID : userSID,
			}, // parameters
			success : function(response) {
				console.log(response);
				unregister();
			}
		});
	}

	function getCorrectGoogleMapsSrc(_hospitalSID) {
		// klinikum rechts der isar
		if (_hospitalSID === "733aafb3-1082-4278-a89c-a3a6bc91c240") {
			return "https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d85096.4594339842!2d11.571632421489404!3d48.201523796220464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x479e72ece78d321f%3A0xf8d2874f0eb7c24c!2sGarching+-+Forschungszentrum%2C+Garching!3m2!1d48.265043299999995!2d11.6715693!4m5!1s0x479e7582085e4a3f%3A0xa3dffab473c868fe!2sKlinikum+rechts+der+Isar+der+Technischen+Universit%C3%A4t+M%C3%BCnchen%2C+Ismaninger+Str.+22%2C+81675+M%C3%BCnchen!3m2!1d48.137500499999994!2d11.601051!5e0!3m2!1sen!2sde!4v1559395754495!5m2!1sen!2sde";
		}

		// Klinikum Bogenhausen
		else if (_hospitalSID === "5c899ed5-aa69-4d25-b291-e440fa490a13") {
			return "https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d42586.99610793185!2d11.5751878!3d48.1548343!3m2!1i1024!2i768!4f13.1!4m11!3e3!4m5!1s0x479e728d1f9699f1%3A0x9da9b0e786a59180!2sTUM+Entrepreneurship+Research+Institute%2C+Lichtenbergstra%C3%9Fe+6%2C+85748+Garching+bei+M%C3%BCnchen%2C+Deutschland!3m2!1d48.2679842!2d11.6658592!4m3!3m2!1d48.1558687!2d11.6238722!5e0!3m2!1sde!2sde!4v1561552119664!5m2!1sde!2sde";
		}

		// Klinikum der Ludwig-Maximilians-Universität München
		else if (_hospitalSID === "68988b34-8f53-4d27-9cda-52c1758a1a7c") {
			return "https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d170236.94663486068!2d11.43084326287859!3d48.188270707144895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e3!4m5!1s0x479e728d1f9699f1%3A0x9da9b0e786a59180!2sTUM+Entrepreneurship+Research+Institute%2C+Lichtenbergstra%C3%9Fe+6%2C+85748+Garching+bei+M%C3%BCnchen!3m2!1d48.2679842!2d11.6658592!4m5!1s0x479dd84e174f70c5%3A0xbe2856eb1d70bdbb!2sKlinikum+der+Universit%C3%A4t+M%C3%BCnchen%2C+Marchioninistra%C3%9Fe+15%2C+81377+M%C3%BCnchen!3m2!1d48.1106346!2d11.469980099999999!5e0!3m2!1sde!2sde!4v1561552092479!5m2!1sde!2sde";
		}

		// Klinikum Schwabing
		else {
			return "https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d85074.28714618628!2d11.55235568426404!3d48.21486948011094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e3!4m5!1s0x479e728d1f9699f1%3A0x9da9b0e786a59180!2sTUM+Entrepreneurship+Research+Institute%2C+Lichtenbergstra%C3%9Fe+6%2C+85748+Garching+bei+M%C3%BCnchen!3m2!1d48.2679842!2d11.6658592!4m5!1s0x479e75cdaaee0861%3A0x43a3c916b761c898!2sNotaufnahme+-+St%C3%A4dtisches+Klinikum+M%C3%BCnchen-Klinikum+Schwabing+Klinik%2C+House+5+Ground+floor%2C+K%C3%B6lner+Pl.+1%2C+80804+M%C3%BCnchen!3m2!1d48.17196!2d11.5792349!5e0!3m2!1sde!2sde!4v1561552015133!5m2!1sde!2sde"
		}
	}

	function getAllERUsers() {
		$.ajax({
			type : "POST",
			url : "/TechChallenge/ajaxGetAllUsersInER",
			data : {
				hospitalSID : hospitalSID,
				userSID : userSID,
			}, // parameters
			success : function(response) {
				displayAllUsers(response);
			}
		});
	}

	function displayAllUsers(response) {
		var $listDomElem = $("#listOtherUsers");
		$listDomElem.empty();

		response
				.forEach(function(item, index) {
					// for the current user
					var time = moment(item.estTreatBegin, "DD MMM YYYY HH:mm")
							.toDate();
					time = time.toLocaleTimeString().substring(0, 5);
					if (userERNumber == item.userERNumber) {
						$listDomElem
								.append('<tr style=\"font-weight: bold; color: red;\"><th scope="row">'
										+ item.userERNumber
										+ '</th><td>'
										+ time + '</td></tr>');

						// for everyone else
					} else {
						$listDomElem.append('<tr><th scope="row">'
								+ item.userERNumber + '</th><td>' + time
								+ '</td></tr>');
					}
				});
	}

	/*
	 * cookie lifetime has to be a date object
	 */
	function createERCookies(_userSID, _hospitalSID, _userERNumber,
			_hospitalName, _cookieLifeTime, _estTreatBegin) {
		Cookies.set("userSID", _userSID, _cookieLifeTime);
		Cookies.set("hospitalSID", _hospitalSID, _cookieLifeTime);
		Cookies.set("userERNumber", _userERNumber, _cookieLifeTime);
		Cookies.set("hospitalName", _hospitalName, _cookieLifeTime);
		Cookies.set("estTreatBegin", _estTreatBegin, _cookieLifeTime);
	}

	function deleteAllERCookies() {
		Cookies.remove("userSID");
		Cookies.remove("hospitalSID");
		Cookies.remove("userERNumber");
		Cookies.remove("hospitalName");
		Cookies.remove("estTreatBegin");
	}

	return {
		initWaitingRoom : init,
		register : register,
		sendRegisterRequest : sendRegisterRequest,
		sendUnregisterRequest : sendUnregisterRequest,
		getAllERUsers : getAllERUsers,
	}
})();

$(document).ready(function() {
	var navbarCollapse = function() {
		if ($("#mainNav").offset().top > 100) {
			$("#mainNav").addClass("navbar-shrink");
		} else {
			$("#mainNav").removeClass("navbar-shrink");
		}
	};
	// Collapse now if page is not at top
	navbarCollapse();
	// Collapse the navbar when page is scrolled
	$(window).scroll(navbarCollapse);

	waitingRoom.initWaitingRoom();
});


var waitingRoom = (function() {
	var $EROptionsContainer;
	var $ERContainer;
	var $UsersContainer;

	var hospitalSID = null;
	
	var interval;

	function init() {
		$EROptionsContainer = $('#EROptionsContainer');
		$ERContainer = $('#ERContainer');
		$UsersContainer = $('#usersContainer');
	}

	function selectHospital(_hospitalSID) {
		hospitalSID = _hospitalSID;
		
		$EROptionsContainer.hide();
		$ERContainer.show();
		
		getAllERUsers();
		interval = setInterval(getAllERUsers,2000);
		
		$("#mainBody").css("background-color","#1abc9c");
	}

	function getAllERUsers() {
		$.ajax({
			type : "POST",
			url : "/TechChallenge/ajaxGetAllUsersInER",
			data : {
				hospitalSID : hospitalSID,
			}, // parameters
			success : function(response) {
				displayAllUsers(response);
			}
		});
	}

	function displayAllUsers(response) {
		$UsersContainer.empty();

		response
				.forEach(function(item, index) {
					var time = moment(item.estTreatBegin, "DD MMM YYYY HH:mm").toDate();
					time = time.toLocaleTimeString().substring(0, 5);
					$UsersContainer.append(
							'<tr><th scope="row">'+ item.userERNumber + '</th><td>' + time + '</td></tr>');
				});
	}

	return {
		initWaitingRoom : init,
		selectHospital : selectHospital
	}
})();

$(document).ready(function() {
	waitingRoom.initWaitingRoom();
});

